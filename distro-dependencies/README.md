When adding new packages, please keep the listing in alphabetical order.

If using JetBrains IDE, select the wanted lines, then in main menu: **Edit | Sort Lines**.

## Fedora

In case you are adding a `*-devel` package for a program, please consider adding
it as a virtual package matching the resource requested by the build script.

For example, if a CMakeLists has the following:

```cmake
find_package(Qt6 ${QT_MIN_VERSION} CONFIG REQUIRED COMPONENTS
    Core
    DBus
)

pkg_check_modules(PipeWire IMPORTED_TARGET libpipewire-0.3)
```

Then instead you want to add the following:
```ini
cmake(Qt6Core)
cmake(Qt6DBus)
pkgconfig(libpipewire-0.3)
```

These are virtual package names matching to development packages in the distribution.

If Python modules are needed and they are typically published to PyPI (e.g. `sphinx-rtd-theme`
for `sphinx`), then use `python3dist()` virtual names (e.g. `python3dist(sphinx-rtd-theme)`).
Similar things exist for Perl modules (`perl()`), RubyGems (`rubygem()`), Rust crates (`crate()`),
and so on.

You can also request executables by their full filepath if you need to, but for this
we recommend using package names. For example, if you need `/usr/bin/sphinx-build`,
you can find out by querying for it:
```bash
dnf repoquery --file '/usr/bin/sphinx-build'
```

You will see output like the following:
```
python3-sphinx-1:7.3.7-2.fc41.noarch
```

This can also be useful if your software build script looks for specific files rather
than using CMake configuration modules or pkg-config modules.

If your build script looks for specific files where either CMake or pkg-config resources
are available to identify it instead, please consider updating your build scripts to use
either of those and adjust accordingly. This is more portable and saves you hassles down
the road for building the application as resources can and do shift around.

For example, if your build script requires `/usr/bin/wayland-scanner`, you can identify
that it comes from `wayland-devel` using the command above. But now you want to know what
resources it has to offer, and you can find out with this command:
```bash
dnf repoquery --provides wayland-devel
```

You will see output like this:
```
pkgconfig(wayland-client) = 1.23.0
pkgconfig(wayland-cursor) = 1.23.0
pkgconfig(wayland-egl) = 18.1.0
pkgconfig(wayland-egl-backend) = 3
pkgconfig(wayland-scanner) = 1.23.0
pkgconfig(wayland-server) = 1.23.0
wayland-devel = 1.23.0-2.fc41
wayland-devel(x86-32) = 1.23.0-2.fc41
wayland-devel(x86-64) = 1.23.0-2.fc41
```

You can see that `pkgconfig(wayland-scanner)` is an option. But since you care about the
executable, you may want to know how to find it from pkg-config to use in your build script.

Presuming you installed `wayland-devel`, you can query it using `pkg-config`:
```bash
pkg-config --print-variables wayland-scanner
```

You will see output like the following:
```
wayland_scanner
bindir
pkgdatadir
datarootdir
includedir
prefix
pcfiledir
```

The `wayland_scanner` variable looks interesting, so check it specifically:
```bash
pkg-config wayland-scanner --variable=wayland_scanner
```
And you get the following output:
```
/usr/bin/wayland-scanner
```

This can be used in CMake like so:

```cmake
pkg_get_variable(WAYLAND_SCANNER_EXECUTABLE wayland-scanner wayland_scanner)
```

## openSUSE

In case you are adding a `*-devel` package, for example, `qt6-pdf-devel`, prefer adding it
as a virtual package. Run the command:
```bash
sudo zypper info --provides qt6-pdf-devel
```

You will see output like this:
```
Information for package qt6-pdf-devel:
--------------------------------------
Repository     : openSUSE-Tumbleweed-Oss
Name           : qt6-pdf-devel
Version        : 6.7.0-1.1
Arch           : x86_64
Vendor         : openSUSE
Installed Size : 94,0 KiB
Installed      : No
Status         : not installed
Source package : qt6-webengine-6.7.0-1.1.src
Upstream URL   : https://www.qt.io
Summary        : Development files for the Qt 6 Pdf library
Description    : 
    Development files for the Qt 6 Pdf library.
Provides       : [4]
    cmake(Qt6Pdf) = 6.7.0
    pkgconfig(Qt6Pdf) = 6.7.0
    qt6-pdf-devel = 6.7.0-1.1
    qt6-pdf-devel(x86-64) = 6.7.0-1.1
```

Notice that `cmake(Qt6Pdf)` and `pkgconfig(Qt6Pdf)` virtual packages.

So instead of adding `qt6-pdf-devel`, add `cmake(Qt6Pdf)`.

`cmake(xxx)` is for packages providing CMake config files (minus private-devel packages for Qt)  
`pkgconfig(xxx)` is for packages only providing pkgconfig files.  
There is a benefit for these: if a package gets renamed, the cmake / pkgconfig names would not change.  

If you know the cmake() or pkgconfig() name you need to find, but are unsure of the -devel package name, you can run `zypper se --provides 'cmake(foo)'` or `zypper se --provides 'pkgconfig(foo)'` 

In general, there is a preference for using the cmake() virtual packages where possible.
